# Raspberry Pi Polybar

### Installation Guide
Steps to install Polybar on Raspberry Pi
1.  Install dependencies* `sudo apt-get install cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev libxcb-composite0-dev`
2.  clone polybar git repo [here](https://github.com/polybar/polybar).
3.  `cd polybar && ./build.sh`
(Type whatever for 'Use GCC even if Clang is installed' and 'n' for the rest)**

### How to Add Custom Modules to Polybar
1.  Create directory to store script files.
2.  If script is not able to be executed run `chmod +x *script.sh*`
3.  In polybar config file (typically ~/.config/polybar/config) specify type as custom script with `type = custom/script`
4.  To execute the script enter line `exec = /path/to/directory/script.sh`
5.  If you wish to execute the script with a user action (see [here](https://github.com/polybar/polybar/wiki/Configuration) for more info) add after user action `exec = /path/to/directory/script.sh`

### Custom Scripts to replace modules there is not support for

#### Fonts Necessary:
*  fonts-font-awesome (sudo apt install fonts-font-awesome)
*  [font-logos](https://github.com/lukas-w/font-logos)

#### pi-logo

##### Features:
*  Displays raspberry pi logo
*  Shutdown on left click
*  Reboot on right click

##### Sample Config

[module/pi-logo]

type = custom/text

;#BD0840 is color of raspberry pi logo, adjust hex value for different colors

content = %{F#BD0840}

;make font be whichever number you assign to font-logos. Unicode for pi logo is U+F115

content-font = 3

click-left = exec sudo shutdown now

click-right = exec sudo reboot now

#### pifi (replaces network  module)

##### Features:
*  When connected displays wifi symbol(that I will add here later) along with SSID of network.
*  When disconnected displays '—' because I can't find a wifi disconnected symbol in Font Awesome

*Future additions*

* [ ] Toggle wifi on and off with left-click. (can't find command that is able to reliably stop and start wifi)
* [ ] Open Network GUI with right-click.
* [ ] Compatibility with both wpa_supplicant and NetworkManager.


##### Sample config

[module/pifi]

type = custom/script

interval = 5

exec = /path/to/directory/pifi.sh

format-padding = 3

#### pivolume (replaces alsa module)

##### Features:
*  Displays volume symbol along with percent.
*  Adjust volume by scrolling up or down while hovering over.

*Future additions*

* [x] Mute on right click
* [ ] Display slider on left click


##### Sample config

[module/pivolume]

type = custom/script

interval = 0.2

exec = /path/to/directory/volume.sh

format-padding = 2

;for some reason raising or lowering percent is done in binary meaning 101%=5%

scroll-down = exec amixer sset PCM 101%-

scroll-up = exec amixer sset PCM 101%+

click-right = exec /path/to/directory/toggle_pivolume.sh